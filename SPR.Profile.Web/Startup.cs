using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SPR.Profile.Abstractions;
using SPR.Profile.Mapping;
using SPR.Profile.Services;
using SPR.Profile.SwaggerClient;
using SPR.Profile.Web.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VNogin.HttpClientHandlers;

namespace SPR.Profile.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();

            var identityServerSettings = Configuration.GetSection("IdentityServer").Get<IdentityServerSettings>();

            AddIdentityServer();
            AddHealthChecks();

            services.AddAccessTokenManagement();
            services.AddDefaultConfigurationHttpClientPolicies(Configuration);

            services
                .AddHttpClient<IProfileClient, ProfileClient>(client => {
                    client.BaseAddress = identityServerSettings.BaseUrl;
                })
                .AddUserAccessTokenHandler()
                .AddLoggingHandler()
                .AddConfigurationPolicies();

            services.AddTransient<IProfileService, ProfileService>();
            services.AddAutoMapper(typeof(MappingProfile));

            services.ConfigureNonBreakingSameSiteCookies();

            void AddIdentityServer()
            {
                services
                    .AddAuthentication(options => {
                        options.DefaultScheme = "Cookies";
                        options.DefaultChallengeScheme = "oidc";
                    })
                    .AddCookie("Cookies", options => {
                        options.Cookie.SecurePolicy = Microsoft.AspNetCore.Http.CookieSecurePolicy.Always;
                        options.Cookie.SameSite = Microsoft.AspNetCore.Http.SameSiteMode.None;
                    })
                    .AddOpenIdConnect("oidc", options => {
                        options.Authority = identityServerSettings.Authority;
                        options.RequireHttpsMetadata = identityServerSettings.RequireHttpsMetadata;

                        options.ClientId = "profile";
                        options.ClientSecret = identityServerSettings.ClientSecret ?? "drinkingwater";
                        options.ResponseType = "code";

                        options.SaveTokens = true;
                        options.UseTokenLifetime = true;

                        options.TokenValidationParameters.ClockSkew = TimeSpan.FromSeconds(0);

                        options.Scope.Add("openid");
                        options.Scope.Add("profile");
                        options.Scope.Add("offline_access");
                        options.Scope.Add("users_api.profile");
                    });
            }

            void AddHealthChecks()
            {
                services.AddHealthChecks()
                    .AddIdentityServer(identityServerSettings.BaseUrl);
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            } else {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHealthChecks("/health/lite", new HealthCheckOptions {
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse,
            });
            app.UseHealthChecks("/health", new HealthCheckOptions {
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse,
            });

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints => {
                endpoints.MapRazorPages().RequireAuthorization();
            });
        }
    }
}
