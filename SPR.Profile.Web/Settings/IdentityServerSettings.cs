﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SPR.Profile.Web.Settings
{
    public class IdentityServerSettings
    {
        [Required]
        public Uri BaseUrl { get; set; } = null!;

        [Required]
        public string Authority { get; set; } = null!;

        public bool RequireHttpsMetadata { get; set; } = true;

        public string ClientSecret { get; set; }
    }
}
