using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SPR.Profile.Abstractions;

namespace SPR.Profile.Web.Pages
{
    public class ChangePasswordModel : PageModel
    {
        private readonly IProfileService _profile;

        public ChangePasswordModel(IProfileService profile)
        {
            _profile = profile ?? throw new ArgumentNullException(nameof(profile));
        }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
                return Page();

            try {
                await _profile.ChangePasswordAsync(new DataModel.ChangePasswordModel {
                    NewPassword = Model.NewPassword,
                    OldPassword = Model.OldPassword
                });
            } catch (Exceptions.ModelErrorException e) {
                foreach (var error in e.Errors)
                    ModelState.AddModelError($"{nameof(Model)}.{error.Key}", error.Value);                    
                return Page();
            }

            return RedirectToPage(nameof(IndexModel)[0..^5]);
        }

        [BindProperty]
        public ViewModel Model { get; set; }        

        public class ViewModel
        {            
            [Display(Name = "Old password")]
            public string OldPassword { get; set; }

            [Display(Name = "New password")]
            [Required]
            public string NewPassword { get; set; }

            [Display(Name = "Repeat new password")]
            [Required]
            [Compare(nameof(NewPassword))]
            public string RepeatPassword { get; set; }
        }
    }
}
