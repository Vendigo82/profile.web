using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using SPR.Profile.Abstractions;

namespace SPR.Profile.Web.Pages
{
    public class EditModel : PageModel
    {
        private readonly IProfileService _profile;

        public EditModel(IProfileService profile)
        {
            _profile = profile ?? throw new ArgumentNullException(nameof(profile));
        }

        public async Task OnGetAsync()
        {
            var model = await _profile.GetProfileAsync();
            Model = new ViewModel { Name = model.Name };
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
                return Page();

            await _profile.UpdateProfileAsync(new DataModel.UpdateProfileModel { Name = Model.Name });
            return RedirectToPage("Index");
        }

        [BindProperty]
        public ViewModel Model { get; set; }

        public class ViewModel
        {
            public string Name { get; set; }
        }
    }
}
