﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using SPR.Profile.Abstractions;
using SPR.Profile.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.Profile.Web.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly IProfileService _profile;

        public IndexModel(IProfileService profile, ILogger<IndexModel> logger)
        {
            _profile = profile ?? throw new ArgumentNullException(nameof(profile));
            _logger = logger;
        }

        public ProfileModel Profile { get; private set; }

        public IEnumerable<ClientModel> Clients { get; private set; }

        public bool CanChangePassword { get; private set; }

        public async Task<IActionResult> OnGetAsync()
        {
            var dataTask = _profile.GetProfileAsync();
            var clientsTask = _profile.GetClientsAsync();
            await Task.WhenAll(dataTask, clientsTask);

            Profile = dataTask.Result;
            Clients = clientsTask.Result;

            CanChangePassword = dataTask.Result.LoginType == LoginTypes.Local;

            return Page();
        }
    }
}
