﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SPR.Profile.Web.Pages
{
    public class PrivacyModel : PageModel
    {
        private readonly ILogger<PrivacyModel> _logger;

        public PrivacyModel(ILogger<PrivacyModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
            //var accessToken = await HttpContext.GetTokenAsync("access_token");

            //ViewData["UserId"] = User.Claims.Where(i => i.Type == ClaimTypes.NameIdentifier).FirstOrDefault()?.Value;
            //ViewData["UserIdentity.Name"] = User.Identity?.Name;
            //ViewData["AccessToken"] = accessToken;
        }
    }
}
