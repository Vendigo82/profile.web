﻿using SPR.Profile.DataModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SPR.Profile.Abstractions
{
    public interface IProfileService
    {
        public Task<ProfileModel> GetProfileAsync();

        public Task<IEnumerable<ClientModel>> GetClientsAsync();

        public Task UpdateProfileAsync(UpdateProfileModel profile);

        public Task ChangePasswordAsync(ChangePasswordModel model);
    }
}
