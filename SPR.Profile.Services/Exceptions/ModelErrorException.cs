﻿using System;
using System.Collections.Generic;

namespace SPR.Profile.Exceptions
{
    public class ModelErrorException : Exception
    {
        public ModelErrorException(IEnumerable<KeyValuePair<string, string>> errors)
        {
            Errors = errors ?? throw new ArgumentNullException(nameof(errors));
        }

        public IEnumerable<KeyValuePair<string, string>> Errors { get; }
    }
}
