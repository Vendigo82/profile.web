﻿using System.ComponentModel.DataAnnotations;

namespace SPR.Profile.DataModel
{
    public class ChangePasswordModel
    {
        public string? OldPassword { get; set; }

        [Required]
        public string? NewPassword { get; set; }
    }
}
