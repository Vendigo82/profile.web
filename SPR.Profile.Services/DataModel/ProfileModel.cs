﻿using System;

namespace SPR.Profile.DataModel
{
    public enum LoginTypes { Local, Domain }

    public class ProfileModel
    {
        public Guid Id { get; set; }

        public string? Name { get; set; }

        public string Login { get; set; } = null!;

        public LoginTypes LoginType { get; set; }
    }
}
