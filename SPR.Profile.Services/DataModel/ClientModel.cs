﻿using System;

namespace SPR.Profile.DataModel
{
    public class ClientModel
    {
        public Guid Id { get; set; }

        public string SystemName { get; set; } = null!;

        public string Title { get; set; } = null!;
    }
}
