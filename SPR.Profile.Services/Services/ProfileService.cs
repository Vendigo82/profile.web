﻿using AutoMapper;
using SPR.Profile.Abstractions;
using SPR.Profile.DataModel;
using SPR.Profile.Extensions;
using SPR.Profile.SwaggerClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPR.Profile.Services
{
    public class ProfileService : IProfileService
    {
        private readonly IProfileClient _client;
        private readonly IMapper _mapper;

        public ProfileService(IProfileClient client, IMapper mapper)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<ClientModel>> GetClientsAsync()
        {
            var response = await _client.ClientsAsync();
            return _mapper.Map<IEnumerable<ClientModel>>(response.Items);
        }

        public async Task<ProfileModel> GetProfileAsync()
        {
            var response = await _client.ProfileGetAsync();
            return _mapper.Map<ProfileModel>(response);
        }

        public Task UpdateProfileAsync(UpdateProfileModel profile)
        {
            var model = _mapper.Map<UserProfileUpdateDto>(profile);
            return _client.ProfilePostAsync(model);
        }

        public async Task ChangePasswordAsync(ChangePasswordModel model)
        {
            var request = _mapper.Map<ProfileChangePasswordRequest>(model);
            try {
                await _client.PasswordAsync(request);
            } catch (ApiException<ValidationProblemDetails> e) when (e.StatusCode == 400) {
                throw new Exceptions.ModelErrorException(e.Result.Errors!.SelectMany(i => i.Value.Select(error => new KeyValuePair<string, string>(ChangeKey(i.Key), error))));
            }

            string ChangeKey(string key) {
                if (key == request.GetJsonPropertyName(p => p.OldPassword))
                    return nameof(model.OldPassword);
                if (key == request.GetJsonPropertyName(p => p.Password))
                    return nameof(model.NewPassword);
                return key;
            };
        }
    }
}
