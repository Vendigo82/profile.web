﻿using AutoMapper;
using AutoMapper.Extensions.EnumMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Profile.Mapping
{
    public class MappingProfile : AutoMapper.Profile
    {
        public MappingProfile()
        {
            CreateMap<SwaggerClient.LoginTypes, DataModel.LoginTypes>().ConvertUsingEnumMapping(s => s.MapByName());
            CreateMap<SwaggerClient.UserDto, DataModel.ProfileModel>();
            CreateMap<SwaggerClient.ClientDto, DataModel.ClientModel>();
            CreateMap<DataModel.UpdateProfileModel, SwaggerClient.UserProfileUpdateDto>();
            CreateMap<DataModel.ChangePasswordModel, SwaggerClient.ProfileChangePasswordRequest>()
                .ForMember(d => d.Password, m => m.MapFrom(s => s.NewPassword));
        }
    }
}
