﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace SPR.Profile.Extensions
{
    public static class JsonModelExtensions
    {
#pragma warning disable IDE0060 // Remove unused parameter
        public static string GetJsonPropertyName<TSource, TProperty>(this TSource source,
#pragma warning restore IDE0060 // Remove unused parameter
            Expression<Func<TSource, TProperty>> propertyLambda) => GetJsonPropertyName(propertyLambda);

        public static string GetJsonPropertyName<TSource, TProperty>(//TSource source,
            Expression<Func<TSource, TProperty>> propertyLambda)
        {
            Type type = typeof(TSource);

            MemberExpression member = propertyLambda.Body as MemberExpression ?? throw new NullReferenceException();
            if (member == null)
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a method, not a property.",
                    propertyLambda.ToString()));

            PropertyInfo propInfo = member.Member as PropertyInfo ?? throw new NullReferenceException();
            if (propInfo == null)
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a field, not a property.",
                    propertyLambda.ToString()));

            if (type != propInfo.ReflectedType && !type.IsSubclassOf(propInfo.ReflectedType!))
                throw new ArgumentException(string.Format(
                    "Expression '{0}' refers to a property that is not from type {1}.",
                    propertyLambda.ToString(),
                    type));

            var attr = propInfo.GetCustomAttributes<JsonPropertyAttribute>().FirstOrDefault();
            if (attr != null && attr.PropertyName != null)
                return attr.PropertyName;

            return propInfo.Name;
        }
    }
}
