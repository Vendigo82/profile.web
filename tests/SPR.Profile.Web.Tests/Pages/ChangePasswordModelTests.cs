﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Moq;
using SPR.Profile.Abstractions;
using SPR.Profile.Exceptions;
using SPR.Profile.Web.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Profile.Web.Tests.Pages
{
    public class ChangePasswordModelTests
    {
        readonly Mock<IProfileService> serviceMock = new();
        readonly ChangePasswordModel pageModel;

        public ChangePasswordModelTests()
        {
            pageModel = new(serviceMock.Object);
        }

        [Theory, AutoData]
        public async Task PostSuccessTest(ChangePasswordModel.ViewModel model)
        {
            // setup
            pageModel.Model = model;

            // action
            var result = await pageModel.OnPostAsync();

            // asserts
            result.Should().BeOfType<RedirectToPageResult>().Which.PageName.Should().Be("Index");

            var args = new List<DataModel.ChangePasswordModel>();
            serviceMock.Verify(f => f.ChangePasswordAsync(Capture.In(args)), Times.Once);
            var arg = args.Should().ContainSingle().Which;
            model.Should().BeEquivalentTo(arg);
        }

        [Theory, AutoData]
        public async Task PostModelErrorException(ChangePasswordModel.ViewModel model, ModelErrorException exception)
        {
            // setup
            pageModel.Model = model;
            serviceMock.Setup(f => f.ChangePasswordAsync(It.IsAny<DataModel.ChangePasswordModel>())).ThrowsAsync(exception);

            // action
            var result = await pageModel.OnPostAsync();

            // asserts
            result.Should().BeOfType<PageResult>().Which.Page.Should().BeNull();
            pageModel.ModelState.IsValid.Should().BeFalse();
            pageModel.ModelState.SelectMany(i => i.Value.Errors.Select(e => new { Key = i.Key, Value = e.ErrorMessage }))
                .Should().BeEquivalentTo(exception.Errors.Select(i => new { Key = "Model." + i.Key, i.Value }));                
        }
    }
}
