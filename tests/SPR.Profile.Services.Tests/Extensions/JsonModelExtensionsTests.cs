﻿using FluentAssertions;
using Newtonsoft.Json;
using SPR.Profile.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Profile.Services.Tests.Extensions
{
    public class JsonModelExtensionsTests
    {
        public class Model
        {
            public string WithoutAttribute { get; set; }

            [JsonProperty]
            public string AttrWithoutName { get; set; }

            [JsonProperty(PropertyName = "SomeName")]
            public string AttrWithName { get; set; }
        }

        [Theory, MemberData(nameof(TestCases))]        
        public void GetJsonPropertyName(Expression<Func<Model, string>> propertyLambda, string expected)
        {
            // setup
            var a = (Expression<Func<Model, string>>)(m => m.AttrWithName);

            // action
            var result = JsonModelExtensions.GetJsonPropertyName(propertyLambda);

            // asserts
            result.Should().Be(expected);
        }

        public static IEnumerable<object[]> TestCases {
            get {
                yield return new object[] { (Expression<Func<Model, string>>)(m => m.AttrWithName), "SomeName" };
                yield return new object[] { (Expression<Func<Model, string>>)(m => m.AttrWithoutName), nameof(Model.AttrWithoutName) };
                yield return new object[] { (Expression<Func<Model, string>>)(m => m.WithoutAttribute), nameof(Model.WithoutAttribute) };
            }
        }
    }
}
