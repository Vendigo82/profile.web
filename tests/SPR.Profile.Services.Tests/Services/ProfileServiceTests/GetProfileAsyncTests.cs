﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using SPR.Profile.SwaggerClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Profile.Services.Tests.Services.ProfileServiceTests
{
    public class GetProfileAsyncTests : ProfileServiceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(UserDto clientResponse)
        {
            // setup
            clientMock.Setup(f => f.ProfileGetAsync(default)).ReturnsAsync(clientResponse);

            // action
            var result = await service.GetProfileAsync();

            // asserts
            clientResponse.Should().BeEquivalentTo(result);
            clientMock.Verify(f => f.ProfileGetAsync(default), Times.Once);
            clientMock.VerifyNoOtherCalls();
        }
    }
}
