﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using SPR.Profile.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Profile.Services.Tests.Services.ProfileServiceTests
{
    public class UpdateProfileAsyncTests : ProfileServiceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(UpdateProfileModel model)
        {
            // setup

            // action
            await service.UpdateProfileAsync(model);

            // asserts
            var args = new List<SwaggerClient.UserProfileUpdateDto>();
            clientMock.Verify(f => f.ProfilePostAsync(Capture.In(args), default), Times.Once);
            var requestModel = args.Should().ContainSingle().Which;
            model.Should().BeEquivalentTo(requestModel);
        }
    }
}
