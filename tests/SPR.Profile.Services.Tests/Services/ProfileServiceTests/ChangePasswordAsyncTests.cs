﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Profile.Services.Tests.Services.ProfileServiceTests
{
    public class ChangePasswordAsyncTests : ProfileServiceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(DataModel.ChangePasswordModel model)
        {
            // setup

            // action
            await service.ChangePasswordAsync(model);

            // asserts
            var args = new List<SwaggerClient.ProfileChangePasswordRequest>();
            clientMock.Verify(f => f.PasswordAsync(Capture.In(args), default), Times.Once);
            var requestModel = args.Should().ContainSingle().Which;
            requestModel.Should().BeEquivalentTo(model, o => o.Excluding(p => p.NewPassword));
            requestModel.Password.Should().Be(model.NewPassword);
        }

        [Theory]
        [InlineAutoData("password", nameof(DataModel.ChangePasswordModel.NewPassword))]
        [InlineAutoData("oldPassword", nameof(DataModel.ChangePasswordModel.OldPassword))]
        [InlineAutoData("xxx", "xxx")]
        public async Task BadRequestTest(string propertyName, string expectedPropertyName, string errorMessage)
        {
            // setup
            var errors = new Dictionary<string, ICollection<string>> {
                { propertyName, new []{ errorMessage } }
            };
            var details = new SwaggerClient.ValidationProblemDetails { Errors = errors };
            clientMock.Setup(f => f.PasswordAsync(It.IsAny<SwaggerClient.ProfileChangePasswordRequest>(), default))
                .ThrowsAsync(new SwaggerClient.ApiException<SwaggerClient.ValidationProblemDetails>("", 400, "", null, details, null));

            // action
            Func<Task> action = () => service.ChangePasswordAsync(new DataModel.ChangePasswordModel());

            // asserts
            var exception = (await action.Should().ThrowExactlyAsync<Exceptions.ModelErrorException>()).Which;
            exception.Errors.Should()
                .NotBeNullOrEmpty().And
                .ContainSingle().Which.Should()
                .BeEquivalentTo(new { Key = expectedPropertyName, Value = errorMessage });
        }
    }
}
