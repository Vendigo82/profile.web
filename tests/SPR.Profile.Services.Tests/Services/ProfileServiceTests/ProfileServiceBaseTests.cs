﻿using AutoMapper;
using Moq;
using SPR.Profile.Mapping;
using SPR.Profile.SwaggerClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPR.Profile.Services.Tests.Services.ProfileServiceTests
{
    public class ProfileServiceBaseTests
    {
        protected readonly Mock<IProfileClient> clientMock = new();
        protected readonly ProfileService service;

        public ProfileServiceBaseTests()
        {
            var mapper = new Mapper(new MapperConfiguration(c => c.AddProfile<MappingProfile>()));
            service = new ProfileService(clientMock.Object, mapper);
        }
    }
}
