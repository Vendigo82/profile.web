﻿using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using SPR.Profile.SwaggerClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Profile.Services.Tests.Services.ProfileServiceTests
{
    public class GetClientsAsyncTests : ProfileServiceBaseTests
    {
        [Theory, AutoData]
        public async Task SuccessTest(ICollection<ClientDto> clients)
        {
            // setup
            clientMock.Setup(f => f.ClientsAsync(default)).ReturnsAsync(new ClientDtoListContainer { Items = clients });

            // action
            var result = await service.GetClientsAsync();

            // asserts
            clients.Should().BeEquivalentTo(result);
            clientMock.Verify(f => f.ClientsAsync(default), Times.Once);
            clientMock.VerifyNoOtherCalls();
        }
    }
}
