﻿using AutoMapper;
using SPR.Profile.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SPR.Profile.Services.Tests.Mapping
{
    public class MappingProfileTests
    {
        [Fact]
        public void ValidateConfigurationTest()
        {
            var cfg = new MapperConfiguration(c => c.AddProfile<MappingProfile>());
            cfg.AssertConfigurationIsValid();
        }
    }
}
